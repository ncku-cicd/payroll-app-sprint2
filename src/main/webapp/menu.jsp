<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<div class="sidebar-sticky">
    <ul class="nav flex-column">
        <li class="nav-item">
            <a class="nav-link <c:if test="${param.item == 'home'}">active</c:if>" href="home">
                <span data-feather="home"></span>
                Home
            </a>

        </li>
        <li class="nav-item">
            <a class="nav-link <c:if test="${param.item == 'timeCard'}">active</c:if>" href="timeCard">
                <span data-feather="file"></span>
                Time Card
            </a>
        </li>
        <li class="nav-item">
            <a class="nav-link <c:if test="${param.item == 'employee'}">active</c:if>" href="employee">
                <span data-feather="file"></span>
                Employee
            </a>
        </li>
    </ul>
</div>